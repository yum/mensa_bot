-- CreateTable
CREATE TABLE "MensaDay" (
    "id" SERIAL NOT NULL,
    "date" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "MensaDay_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_MealToMensaDay" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_MealToMensaDay_AB_unique" ON "_MealToMensaDay"("A", "B");

-- CreateIndex
CREATE INDEX "_MealToMensaDay_B_index" ON "_MealToMensaDay"("B");

-- AddForeignKey
ALTER TABLE "_MealToMensaDay" ADD CONSTRAINT "_MealToMensaDay_A_fkey" FOREIGN KEY ("A") REFERENCES "Meal"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_MealToMensaDay" ADD CONSTRAINT "_MealToMensaDay_B_fkey" FOREIGN KEY ("B") REFERENCES "MensaDay"("id") ON DELETE CASCADE ON UPDATE CASCADE;
