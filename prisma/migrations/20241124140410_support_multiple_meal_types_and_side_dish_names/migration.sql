/*
  Warnings:

  - You are about to drop the column `mealType` on the `Meal` table. All the data in the column will be lost.
  - You are about to drop the column `name` on the `Meal` table. All the data in the column will be lost.
  - Added the required column `mainDishName` to the `Meal` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Meal" DROP COLUMN "mealType",
DROP COLUMN "name",
ADD COLUMN     "mainDishName" TEXT NOT NULL,
ADD COLUMN     "mealTypes" TEXT[],
ADD COLUMN     "sideDishNames" TEXT[];
