/*
  Warnings:

  - A unique constraint covering the columns `[date]` on the table `MensaDay` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "MensaDay_date_key" ON "MensaDay"("date");
