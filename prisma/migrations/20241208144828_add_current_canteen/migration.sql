/*
  Warnings:

  - Added the required column `currentCanteen` to the `ChannelPost` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "ChannelPost" ADD COLUMN     "currentCanteen" TEXT DEFAULT NULL;
