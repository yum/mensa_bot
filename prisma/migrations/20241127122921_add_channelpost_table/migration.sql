-- CreateTable
CREATE TABLE "ChannelPost" (
    "id" SERIAL NOT NULL,
    "channelId" BIGINT NOT NULL,
    "messageId" INTEGER NOT NULL,
    "date" DATE NOT NULL,
    "markupCleared" BOOLEAN NOT NULL,

    CONSTRAINT "ChannelPost_pkey" PRIMARY KEY ("id")
);
