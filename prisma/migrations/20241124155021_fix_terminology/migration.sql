/*
  Warnings:

  - You are about to drop the column `mainDishName` on the `Meal` table. All the data in the column will be lost.
  - You are about to drop the column `sideDishNames` on the `Meal` table. All the data in the column will be lost.
  - Added the required column `dishName` to the `Meal` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Meal" DROP COLUMN "mainDishName",
DROP COLUMN "sideDishNames",
ADD COLUMN     "dishName" TEXT NOT NULL,
ADD COLUMN     "subDishNames" TEXT[];
