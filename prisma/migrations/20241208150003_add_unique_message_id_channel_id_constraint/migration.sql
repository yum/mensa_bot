/*
  Warnings:

  - A unique constraint covering the columns `[channelId,messageId]` on the table `ChannelPost` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "ChannelPost_channelId_messageId_key" ON "ChannelPost"("channelId", "messageId");
