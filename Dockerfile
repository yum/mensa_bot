# syntax=docker/dockerfile:1
FROM node:alpine
WORKDIR /usr/app
COPY ./ /usr/app

RUN apk add --no-cache tzdata
ENV TZ=Europe/Berlin

RUN npm install
RUN npx prisma migrate deploy
RUN npx tsc

CMD ["node", "./out/bot.js"]
