export interface WeekMenuResponse {
  canteen: Record<string, string>
  menusOfTheDay: MenuOfTheDayResponse[]
}

export interface MenuOfTheDayResponse {
  day: string
  sideDishes: MealResponse[]
  menus: MenuResponse[]
}

export interface MealResponse {
  name: string
  allergens: string[]
  information: string[]
  sideDishType: SideDishType
}

export enum SideDishType {
  Main = 'Main',
  Secondary = 'Secondary',
}

export interface MenuResponse {
  category: string
  price: number | null
  mealTypes: MealType[]
  meals: MealResponse[]
}

export enum MealType {
  Beef = 'Beef',
  Pig = 'Pig',
  Fish = 'Fish',
  Vegetarian = 'Vegetarian',
  Vegan = 'Vegan',
  Poultry = 'Poultry',
  Lamb = 'Lamb',
  Deer = 'Deer',
}

export interface TokenRefreshResponse {
  status: string
  access_token: string
  expires_in: number
  refresh_token: string
  token_type: string
}

export interface TokenValidationResponse {
  status: string
  audience: string | null
  expires_in: number
  scope: string | null
  state: string | null
}
