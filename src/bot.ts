import dotenv from 'dotenv'
import TelegramBot from 'node-telegram-bot-api'
import moment from 'moment-timezone'
import {
  refreshAccessToken,
  requestMenu,
  mealTypeContains,
  unMarkdownify,
  getCanteenIds,
  getReadableCanteenNames,
  convertReadableCanteenToCanteenId,
  getMeanRating,
  refreshChannelPostRating,
  requestAllMenus,
} from './utils.js'
import {
  MealType,
  type MenuResponse,
  type MenuOfTheDayResponse,
  type MealResponse,
  SideDishType,
} from './responses.js'
import * as cron from 'node-cron'
import { PrismaClient } from '@prisma/client'

moment.tz.setDefault('Europe/Berlin')
moment.locale('de-DE')
dotenv.config()

export const bot: TelegramBot = new TelegramBot(
  process.env.TELEGRAM_TOKEN ?? '',
  {
    polling: { interval: 1000 },
  },
)

let accessToken = ''
let awaitingCanteenInput = false
let savedFilter = ''
let savedDate: moment.Moment = moment()
let cooldown = false

const currencyFormatter = Intl.NumberFormat('de-DE', {
  style: 'currency',
  currency: 'EUR',
})

const mealTypeEmojis: Record<MealType, string> = {
  [MealType.Pig]: '🐷',
  [MealType.Poultry]: '🐔',
  [MealType.Beef]: '🐮',
  [MealType.Fish]: '🐟',
  [MealType.Vegetarian]: '🌱',
  [MealType.Vegan]: '🥑',
  [MealType.Lamb]: '🐑',
  [MealType.Deer]: '🦌',
}

const allergenTypeEmojisAndStrings: Record<string, string[]> = {
  // Order: (Emoji), normal script, supscript
  Farbstoff: ['1', '¹'],
  Konservierungsstoff: ['2', '²'],
  Antioxidationsmittel: ['3', '³'],
  Geschmacksverstärker: ['4', '⁴'],
  geschwefelt: ['5', '⁵'],
  geschwärzt: ['6', '⁶'],
  gewachst: ['7', '⁷'],
  Phosphat: ['8', '⁸'],
  Süßungsmittel: ['9', '⁹'],
  'enthält eine Phenylalaninquelle': ['10', '¹⁰'],
  Gluten: ['A', 'ᴬ'],
  Weizen: ['A1', 'ᴬ¹'],
  Roggen: ['A2', 'ᴬ²'],
  Gerste: ['A3', 'ᴬ³'],
  Hafer: ['A4', 'ᴬ⁴'],
  Dinkel: ['A5', 'ᴬ⁵'],
  Sellerie: ['B', 'ᴮ'],
  Krebstiere: ['🦀', 'C', 'ᶜ'],
  Eier: ['🥚', 'D', 'ᴰ'],
  Fische: ['🐟', 'E', 'ᴱ'],
  Erdnüsse: ['🥜', 'F', 'ꟳ'],
  Sojabohnen: ['G', 'ᴳ'],
  Milch: ['🥛', 'H', 'ᴴ'],
  Schalenfrüchte: ['I', 'ᴵ'],
  Mandeln: ['I1', 'ᴵ¹'],
  Haselnüsse: ['I2', 'ᴵ²'],
  Walnüsse: ['🌰', 'I3', 'ᴵ³'],
  Kaschunüsse: ['I4', 'ᴵ⁴'],
  Pecannüsse: ['I5', 'ᴵ⁵'],
  Paranüsse: ['I6', 'ᴵ⁶'],
  Pistazien: ['I7', 'ᴵ⁷'],
  Macadamianüsse: ['I8', 'ᴵ⁸'],
  Senf: ['J', 'ᴶ'],
  Sesamsamen: ['K', 'ᴷ'],
  'Schwefeldioxid oder Sulfite': ['L', 'ᴸ'],
  Lupinen: ['M', 'ᴹ'],
  Weichtiere: ['🐙', 'N', 'ᴺ'],
}

const possibleCommands: string[] = [
  'heute',
  'morgen',
  'uebermorgen',
  'tag',
  'alles',
  'vegan',
  'vegetarisch',
  'license',
  'help',
  'allergene',
  'rate',
  'ratings',
  'start',
]

const helpString =
  '*Hallo!* Ich bin der Mensabot.\n\n' +
  'Kommandos: ' +
  possibleCommands.map((c) => `/${c}`).join(', ') +
  '\n' +
  'Mensa: Direkt spezifizieren mit Abkürzung, z.B. `aca` für Academica\n' +
  'Filter: Auswahl aus: `(' +
  [...Object.keys(mealTypeEmojis), 'Meat', 'Fleisch', 'Vegetarisch'].join(
    ' | ',
  ) +
  ')` \n' +
  'Beispiele:\n' +
  '`/heute aca`\n' +
  '`/morgen ahorn vegan`\n' +
  '`/alles vita vegan`\n' +
  '`/vegan aca` (Abk. für `/heute aca vegan`)\n' +
  '`/morgen`\n' +
  '`/tag Mittwoch aca`\n' +
  '`/tag ' +
  moment().add(1, 'day').format('DD.MM.YY') +
  ' ahorn vegan`\n' +
  '`/rate ' +
  moment().format('DD.MM.YY') +
  '`\n' +
  '`/ratings`'

const possibleCommandsRegex = new RegExp(
  `\\/(${possibleCommands.join('|')})((?: +[^ ]+)*)`,
  'g',
)

accessToken = await refreshAccessToken()

const canteenIds: string[] = await getCanteenIds(accessToken)

const readableCanteenNames: string[] = getReadableCanteenNames(canteenIds)

bot.onText(possibleCommandsRegex, async (msg, match): Promise<void> => {
  awaitingCanteenInput = false
  if (match !== null) {
    switch (match[1]) {
      case 'heute':
        await handleDateCommand('heute', match, msg)
        break
      case 'morgen':
        await handleDateCommand('morgen', match, msg)
        break
      case 'uebermorgen':
        await handleDateCommand('uebermorgen', match, msg)
        break
      case 'vegan':
        await handleFilterCommand('vegan', match, msg)
        break
      case 'vegetarisch':
        await handleFilterCommand('vegetarisch', match, msg)
        break
      case 'alles':
        await handleDateCommand('alles', match, msg)
        break
      case 'tag':
        await handleTagCommand(match, msg)
        break
      case 'rate':
        await handleRateCommand(match, msg)
        break
      case 'ratings':
        await handleRatingsCommand(msg)
        break
      case 'license':
        await bot.sendMessage(
          msg.chat.id,
          'This bot is FOSS! https://git.rwth-aachen.de/yum/mensa_bot',
        )
        break
      case 'help':
      case 'start':
        if (match[2] === '') {
          await bot.sendMessage(msg.chat.id, helpString, {
            parse_mode: 'Markdown',
            reply_to_message_id: msg.message_id,
          })
        } else {
          await handleRateCommand(match, msg)
        }

        break
      case 'allergene':
        await handleAllergensCommand(msg)
    }
  }
})

const requestCanteenName = async (msg: TelegramBot.Message): Promise<void> => {
  await bot.sendMessage(
    msg.chat.id,
    'Bitte Mensanamen auswählen (oder einfach beim nächsten mal dazu schreiben :))',
    {
      reply_to_message_id: msg.message_id,
      reply_markup: {
        keyboard: readableCanteenNames.map((c) => [{ text: c }]),
        one_time_keyboard: true,
      },
    },
  )
}

bot.onText(
  new RegExp(readableCanteenNames.join('|')),
  async (msg, match): Promise<void> => {
    if (awaitingCanteenInput && match !== null) {
      awaitingCanteenInput = false
      const canteenReconverted: string = convertReadableCanteenToCanteenId(
        match[0],
      )
      if (savedFilter !== '') {
        await sendMenu(msg, canteenReconverted, savedDate, savedFilter)
        savedFilter = ''
        savedDate = moment()
      } else {
        await sendMenu(msg, canteenReconverted, savedDate)
        savedDate = moment()
      }
    }
  },
)

bot.on('callback_query', async (cbq) => {
  if (!cooldown) {
    cooldown = true
    const today = moment()

    if (
      cbq.message !== undefined &&
      !cbq.data?.includes('rate') &&
      moment.unix(Number(cbq.message.date)).isSame(today, 'day')
    ) {
      const prisma = new PrismaClient()

      switch (cbq.data) {
        case 'ahorn':
          try {
            if (
              cbq.message.text ===
              unMarkdownify(await getMessageString('Ahornstrasse', today))
            ) {
              await bot.answerCallbackQuery(cbq.id, {
                text: 'Ahorn wird bereits angezeigt.',
              })
            } else {
              try {
                await bot.editMessageText(
                  await getMessageString('Ahornstrasse', today),
                  {
                    parse_mode: 'Markdown',
                    reply_markup: cbq.message.reply_markup,
                    message_id: cbq.message.message_id,
                    chat_id: cbq.message.chat.id,
                  },
                )

                await prisma.channelPost.update({
                  data: { currentCanteen: 'Ahornstrasse' },
                  where: {
                    channelId_messageId: {
                      channelId: cbq.message.chat.id,
                      messageId: cbq.message.message_id,
                    },
                  },
                })
              } catch (e) {
                console.error('Error while editing Channel message', e)
                await bot.answerCallbackQuery(cbq.id, {
                  text: 'Bitte erneut probieren.',
                })
              }
            }
          } catch (e) {
            console.error('Error while editing Channel message', e)
            await bot.answerCallbackQuery(cbq.id, {
              text: 'Bitte erneut probieren.',
            })
          }
          break
        case 'vita':
          try {
            if (
              cbq.message.text ===
              unMarkdownify(await getMessageString('Vita', today))
            ) {
              await bot.answerCallbackQuery(cbq.id, {
                text: 'Vita wird bereits angezeigt.',
              })
            } else {
              try {
                await bot.editMessageText(
                  await getMessageString('Vita', today),
                  {
                    parse_mode: 'Markdown',
                    reply_markup: cbq.message.reply_markup,
                    message_id: cbq.message.message_id,
                    chat_id: cbq.message.chat.id,
                  },
                )

                await prisma.channelPost.update({
                  data: { currentCanteen: 'Vita' },
                  where: {
                    channelId_messageId: {
                      channelId: cbq.message.chat.id,
                      messageId: cbq.message.message_id,
                    },
                  },
                })
              } catch (e) {
                console.error('Error while editing Channel message', e)
                await bot.answerCallbackQuery(cbq.id, {
                  text: 'Bitte erneut probieren.',
                })
              }
            }
          } catch (e) {
            console.error('Error while editing Channel message', e)
            await bot.answerCallbackQuery(cbq.id, {
              text: 'Bitte erneut probieren.',
            })
          }
          break
        case 'aca':
          try {
            if (
              cbq.message.text ===
              unMarkdownify(await getMessageString('Academica', today))
            ) {
              await bot.answerCallbackQuery(cbq.id, {
                text: 'Academica wird bereits angezeigt.',
              })
            } else {
              try {
                await bot.editMessageText(
                  await getMessageString('Academica', today),
                  {
                    parse_mode: 'Markdown',
                    reply_markup: cbq.message.reply_markup,
                    message_id: cbq.message.message_id,
                    chat_id: cbq.message.chat.id,
                  },
                )

                await prisma.channelPost.update({
                  data: { currentCanteen: 'Academica' },
                  where: {
                    channelId_messageId: {
                      channelId: cbq.message.chat.id,
                      messageId: cbq.message.message_id,
                    },
                  },
                })
              } catch (e) {
                console.error('Error while editing Channel message', e)
                await bot.answerCallbackQuery(cbq.id, {
                  text: 'Bitte erneut probieren.',
                })
              }
            }
          } catch (e) {
            console.error('Error while editing Channel message', e)
            await bot.answerCallbackQuery(cbq.id, {
              text: 'Bitte erneut probieren.',
            })
          }
          break
      }
    } else if (cbq.data?.includes('ratestart')) {
      if (cbq.data.split(' ').length > 0) {
        const prisma = new PrismaClient()
        const dish = await prisma.meal.findFirst({
          where: { id: Number(cbq.data.split(' ')[1]) },
        })

        if (dish !== null) {
          const dishName = `${dish.dishName}${dish.subDishNames.length > 0 ? ' mit ' + dish.subDishNames.join(', ') : ''}`
          await bot.answerCallbackQuery(cbq.id)
          await bot.deleteMessage(
            Number(cbq.message?.chat.id),
            Number(cbq.message?.message_id),
          )
          const ratings = [0, 1, 2, 3, 4, 5]

          await bot.sendMessage(
            cbq.from.id,
            `Wie willst du „${dishName}“ bewerten?`,
            {
              reply_markup: {
                inline_keyboard: [
                  [
                    ...ratings.map((rating) => ({
                      text: rating.toString(),
                      callback_data: `rate ${dish.id} ${rating}`,
                    })),
                  ],
                ],
              },
            },
          )
        }
      }
    } else if (cbq.data?.includes('rate')) {
      if (cbq.data.split(' ').length > 1) {
        const prisma = new PrismaClient()
        const dish = await prisma.meal.findFirst({
          where: { id: Number(cbq.data.split(' ')[1]) },
        })
        const rating = Number(cbq.data.split(' ')[2])
        const possibleDuplicateRating = await prisma.rating.findFirst({
          where: { userId: cbq.from.id, dish: { id: dish?.id } },
        })
        const isUpdate = cbq.data.includes('updaterate')

        if (dish !== null) {
          const dishName = `${dish.dishName}${dish.subDishNames.length > 0 ? ' mit ' + dish.subDishNames.join(', ') : ''}`
          await bot.answerCallbackQuery(cbq.id)
          await bot.deleteMessage(
            Number(cbq.message?.chat.id),
            Number(cbq.message?.message_id),
          )

          if (possibleDuplicateRating !== null && !isUpdate) {
            await bot.sendMessage(
              Number(cbq.message?.chat.id),
              `Du hattest „${dishName}“ schon ein mal bewertet. Möchtest du deine Bewertung aktualisieren? 🤔`,
              {
                reply_markup: {
                  inline_keyboard: [
                    [
                      {
                        text: '✅',
                        callback_data: `updaterate ${dish.id} ${rating}`,
                      },
                      {
                        text: '❎',
                        callback_data:
                          'doesntmatterbuttelegramforcesmetogiveavaluehere',
                      },
                    ],
                  ],
                },
              },
            )
          } else {
            await prisma.rating.upsert({
              where: { id: possibleDuplicateRating?.id ?? 0 },
              update: { rating },
              create: {
                rating,
                dish: { connect: { id: dish.id } },
                userId: cbq.from.id,
              },
            })

            await bot.sendMessage(
              Number(cbq.message?.chat.id),
              `Super! Du hast „${dishName}“ mit *${rating}* bewertet!`,
              { parse_mode: 'Markdown' },
            )

            const botUsername = (await bot.getMe()).username

            await refreshChannelPostRating(dish.id, {
              inline_keyboard: [
                [
                  { text: 'Ahorn', callback_data: 'ahorn' },
                  { text: 'Academica', callback_data: 'aca' },
                  { text: 'Vita', callback_data: 'vita' },
                ],
                [
                  {
                    text: 'Mehr Optionen via DMs',
                    url: `tg://resolve?domain=${botUsername}`,
                  },
                ],
                [
                  {
                    text: 'Gericht bewerten',
                    url: `tg://resolve?domain=${botUsername}&start=${today.format('YYYY-MM-DD')}`,
                  },
                ],
              ],
            })
          }
        }
      }
    } else {
      await bot.answerCallbackQuery(cbq.id, {
        text: 'Datum der Nachricht liegt bereits in der Vergangenheit.',
      })
    }
    cooldown = false
  }
})

bot.on('polling_error', async (err) => {
  console.error('Polling Error:', err)
  await bot.stopPolling()
  await new Promise((resolve) => setTimeout(resolve, 5000))
  await bot.startPolling()
})

const sendMenu = async (
  msg: TelegramBot.Message | null,
  canteen: string,
  date: moment.Moment,
  filter?: string,
  markup?: TelegramBot.InlineKeyboardMarkup,
): Promise<TelegramBot.Message[]> => {
  const messageString: string = await getMessageString(canteen, date, filter)
  const splitMessage: string[] = messageString.split('\n').map((l) => `${l}\n`)
  const messages: string[] = []
  const sentMessages: TelegramBot.Message[] = []
  const removeKeyboardMarkup: TelegramBot.ReplyKeyboardRemove = {
    remove_keyboard: true,
  }

  if (messageString.length > 4095) {
    let message = ''
    for (const line of splitMessage) {
      if (message.length + line.length > 4095) {
        messages.push(message)
        message = line
      } else {
        message += line
      }
    }
    messages.push(message)
  } else {
    messages.push(messageString)
  }

  for (const mesg of messages) {
    if (msg !== null) {
      sentMessages.push(
        await bot.sendMessage(msg.chat.id, mesg, {
          parse_mode: 'Markdown',
          reply_to_message_id: msg.message_id,
          reply_markup: removeKeyboardMarkup,
        }),
      )
    } else if (markup !== undefined) {
      for (const channelId of (process.env.CHANNEL_IDS ?? '')
        .split(',')
        .map((c) => c.trim())) {
        sentMessages.push(
          await bot.sendMessage(channelId as TelegramBot.ChatId, mesg, {
            parse_mode: 'Markdown',
            reply_markup: markup,
          }),
        )
      }
    } else {
      for (const channelId of (process.env.CHANNEL_IDS ?? '')
        .split(',')
        .map((c) => c.trim())) {
        sentMessages.push(
          await bot.sendMessage(channelId as TelegramBot.ChatId, mesg, {
            parse_mode: 'Markdown',
            reply_markup: removeKeyboardMarkup,
          }),
        )
      }
    }
  }

  return sentMessages
}

export const getMessageString = async (
  canteen: string,
  date: moment.Moment,
  filter?: string,
): Promise<string> => {
  let menus: MenuOfTheDayResponse[] = []

  if (date.unix() === 0) {
    menus = await requestMenu(accessToken, canteen)
  } else {
    menus = await requestMenu(accessToken, canteen, date)
  }

  let markdownStrings = ''
  let firstString = true

  for (const menu of menus) {
    let relevantMainDishes: MenuResponse[] = []
    let sideDishes: MealResponse[] = []
    date = moment(menu.day, 'YYYY-MM-DD')

    const mainDishesGroupedByType: Record<
      'Vegan' | 'Vegetarian' | 'Else',
      MenuResponse[]
    > = {
      Vegan: [],
      Vegetarian: [],
      Else: [],
    }
    sideDishes = menu.sideDishes

    for (const singleMenu of menu.menus) {
      // skip "geschlossen" dishes
      if (
        !singleMenu.meals.some(
          (meal) =>
            meal.name.toLowerCase().includes('geschlossen') ||
            meal.name.toLowerCase().includes('ausverkauft') ||
            meal.name.toLowerCase().includes('heute kein angebot') ||
            meal.name.toLowerCase().includes('streik'),
        ) &&
        singleMenu.meals.length !== 0
      ) {
        switch (true) {
          case singleMenu.mealTypes.includes(MealType.Vegan):
            mainDishesGroupedByType.Vegan.push(singleMenu)
            break
          case singleMenu.mealTypes.includes(MealType.Vegetarian):
            mainDishesGroupedByType.Vegetarian.push(singleMenu)
            break
          default:
            mainDishesGroupedByType.Else.push(singleMenu)
            break
        }
      }
    }

    // If the StW made an error and did not include mealTypes, we ignore the filter
    if (menu.menus.every((m) => m.mealTypes.length === 0)) {
      filter = undefined
    }

    if (filter !== undefined && filter !== '') {
      switch (filter) {
        case 'vegan':
          relevantMainDishes = mainDishesGroupedByType.Vegan
          break
        case 'vegetarian':
        case 'vegetarisch':
          relevantMainDishes = [
            ...mainDishesGroupedByType.Vegetarian,
            ...mainDishesGroupedByType.Vegan,
          ]
          break
        case 'beef':
          relevantMainDishes = mainDishesGroupedByType.Else.filter((d) =>
            d.mealTypes.includes(MealType.Beef),
          )
          break
        case 'pig':
          relevantMainDishes = mainDishesGroupedByType.Else.filter((d) =>
            d.mealTypes.includes(MealType.Pig),
          )
          break
        case 'poultry':
          relevantMainDishes = mainDishesGroupedByType.Else.filter((d) =>
            d.mealTypes.includes(MealType.Poultry),
          )
          break
        case 'fish':
          relevantMainDishes = mainDishesGroupedByType.Else.filter((d) =>
            d.mealTypes.includes(MealType.Fish),
          )
          break
        case 'lamb':
          relevantMainDishes = mainDishesGroupedByType.Else.filter((d) =>
            d.mealTypes.includes(MealType.Lamb),
          )
          break
        case 'deer':
          relevantMainDishes = mainDishesGroupedByType.Else.filter((d) =>
            d.mealTypes.includes(MealType.Deer),
          )
          break
        case 'fleisch':
        case 'meat':
          relevantMainDishes = mainDishesGroupedByType.Else
          break
      }
    } else {
      relevantMainDishes = [
        ...mainDishesGroupedByType.Vegan,
        ...mainDishesGroupedByType.Vegetarian,
        ...mainDishesGroupedByType.Else,
      ]
    }

    // check for StW errors, indicated by missing mealTypes
    if (menu.menus.every((m) => m.mealTypes.length === 0)) {
      markdownStrings +=
        '_Achtung! Das StW hat scheinbar vergessen, für alle Gerichte für diesen Tag die Ernährungskategorie (z.B. „Vegetarisch“ etc.) einzutragen. Das heißt, dass die nachfolgenden Informationen unvollständig oder fehlerhaft sein können._\n\n'
      markdownStrings +=
        (await constructString(
          canteen,
          date,
          relevantMainDishes,
          sideDishes,
          firstString,
        )) + '\n\n\n'
      firstString = false
      continue
    }

    const markdownString: string = await constructString(
      canteen,
      date,
      relevantMainDishes,
      sideDishes,
      firstString,
    )

    markdownStrings += markdownString + '\n\n\n'
    firstString = false
  }

  if (menus.length === 0) {
    return constructString(canteen, date, [], [], firstString)
  }

  return markdownStrings
}

const constructString = async (
  canteen: string,
  date: moment.Moment,
  mainDishes: MenuResponse[],
  sideDishes: MealResponse[],
  firstString: boolean,
): Promise<string> => {
  let message = ''
  const numberOfDishes: number = mainDishes.length
  const prisma = new PrismaClient()

  if (firstString) {
    // We need to reconvert to human readable name again
    switch (canteen) {
      case 'Templergraben':
        canteen = 'Bistro Templergraben'
        break
      case 'Esstw':
        canteen = 'Esstw'
        break
      default:
        canteen = `Mensa ${canteen.replace('ue', 'ü').replace('strasse', 'straße')}`
        break
    }

    message += `*Plan für ${canteen}*\n\n`
  }

  message += `_${date.format('dddd')}, ${date.format('DD.MM.YYYY')}_\n\n`

  const mainDishesroupedByCategory: Record<string, MenuResponse[]> =
    mainDishes.reduce<Record<string, MenuResponse[]>>((prev, cur) => {
      if (!(cur.category in prev)) {
        prev[cur.category] = [cur]
      } else {
        prev[cur.category].push(cur)
      }
      return prev
    }, {})

  if (mainDishes.some((d) => d.mealTypes.includes(MealType.Vegan))) {
    message += '═══《 🥑 𝐕𝐞𝐠𝐚𝐧 🥑 》═══\n\n'

    let groupedByCategory = Object.values(mainDishesroupedByCategory)
      .sort((a, b) => a[0].category.localeCompare(b[0].category))
      .filter(
        (d) =>
          d.find((dish) => dish.mealTypes.includes(MealType.Vegan)) !==
          undefined,
      )

    for (const group of groupedByCategory) {
      groupedByCategory[groupedByCategory.indexOf(group)] = group.filter((d) =>
        d.mealTypes.includes(MealType.Vegan),
      )
    }

    groupedByCategory = groupedByCategory.filter((g) => g.length !== 0)

    for (const group of groupedByCategory) {
      // Academica Express 0€ price check
      if (
        group[0].category === 'Express' &&
        group[0].price === null &&
        group[0].meals[0].name ===
          mainDishes.find((d) => d.category === 'Klassiker')?.meals[0].name
      ) {
        message += `_${group[0].category}_ — ${currencyFormatter.format(mainDishes.find((d) => d.category === 'Klassiker')?.price ?? 0).replace(/\s/g, '')}\n`
      } else {
        message += `_${group[0].category}_ — ${currencyFormatter.format(group[0].price ?? 0).replace(/\s/g, '')}\n`
      }

      for (const dish of group) {
        let dishString: string =
          '*' +
          dish.meals
            .reduce(
              (prev, cur) =>
                prev +
                cur.name +
                (dish.meals.indexOf(cur) !== 0 ? getAllergenString(cur) : '') +
                ' | ',
              '',
            )
            .replace(/ \| $/gm, '')

        if (dishString.includes(' |')) {
          dishString = dishString.replace(
            ' |',
            '*' + getAllergenString(dish.meals[0]) + ' |',
          )
        } else {
          dishString += '*' + getAllergenString(dish.meals[0])
        }

        message += dishString + ' 🥑'

        const rating = await getMeanRating(prisma, dish)

        message += ` _– ⭐ ${rating.meanRating} (${rating.totalRatings})_\n`
      }
      message += '\n'
    }

    mainDishes = mainDishes.filter((d) => !d.mealTypes.includes(MealType.Vegan))
  }

  if (mainDishes.some((d) => d.mealTypes.includes(MealType.Vegetarian))) {
    message += '═══《 🌱 𝐕𝐞𝐠𝐞𝐭𝐚𝐫𝐢𝐬𝐜𝐡 🌱 》═══\n\n'

    let groupedByCategory = Object.values(mainDishesroupedByCategory)
      .sort((a, b) => a[0].category.localeCompare(b[0].category))
      .filter(
        (d) =>
          d.find(
            (dish) =>
              !dish.mealTypes.includes(MealType.Vegan) &&
              dish.mealTypes.includes(MealType.Vegetarian),
          ) !== undefined,
      )

    for (const group of groupedByCategory) {
      groupedByCategory[groupedByCategory.indexOf(group)] = group.filter(
        (d) =>
          d.mealTypes.includes(MealType.Vegetarian) &&
          !d.mealTypes.includes(MealType.Vegan),
      )
    }

    groupedByCategory = groupedByCategory.filter((g) => g.length !== 0)

    for (const group of groupedByCategory) {
      // Academica Express 0€ price check
      if (
        group[0].category === 'Express' &&
        group[0].price === null &&
        group[0].meals[0].name ===
          mainDishes.find((d) => d.category === 'Klassiker')?.meals[0].name
      ) {
        message += `_${group[0].category}_ — ${currencyFormatter.format(mainDishes.find((d) => d.category === 'Klassiker')?.price ?? 0).replace(/\s/g, '')}\n`
      } else {
        message += `_${group[0].category}_ — ${currencyFormatter.format(group[0].price ?? 0).replace(/\s/g, '')}\n`
      }

      for (const dish of group) {
        let dishString: string =
          '*' +
          dish.meals
            .reduce(
              (prev, cur) =>
                prev +
                cur.name +
                (dish.meals.indexOf(cur) !== 0 ? getAllergenString(cur) : '') +
                ' | ',
              '',
            )
            .replace(/ \| $/gm, '')

        if (dishString.includes(' |')) {
          dishString = dishString.replace(
            ' |',
            '*' + getAllergenString(dish.meals[0]) + ' |',
          )
        } else {
          dishString += '*' + getAllergenString(dish.meals[0])
        }

        message += dishString + ' 🌱'

        const rating = await getMeanRating(prisma, dish)

        message += ` _– ⭐ ${rating.meanRating} (${rating.totalRatings})_\n`
      }
      message += '\n'
    }

    mainDishes = mainDishes.filter(
      (d) => !d.mealTypes.includes(MealType.Vegetarian),
    )
  }

  if (mainDishes.length !== 0) {
    // custom emojis that include everything "else"
    const relevantEmojis: string[] = [
      ...new Set(
        mainDishes.map((d) => d.mealTypes.map((m) => mealTypeEmojis[m])).flat(),
      ),
    ]

    message += `═══《 ${relevantEmojis.join(' ')} 𝐀𝐧𝐝𝐞𝐫𝐞𝐬 ${relevantEmojis.join(' ')} 》═══\n\n`

    let groupedByCategory = Object.values(mainDishesroupedByCategory)
      .sort((a, b) => a[0].category.localeCompare(b[0].category))
      .filter(
        (d) =>
          d.find(
            (dish) =>
              !dish.mealTypes.includes(MealType.Vegan) &&
              !dish.mealTypes.includes(MealType.Vegetarian),
          ) !== undefined,
      )

    for (const group of groupedByCategory) {
      groupedByCategory[groupedByCategory.indexOf(group)] = group.filter(
        (d) =>
          !d.mealTypes.includes(MealType.Vegan) &&
          !d.mealTypes.includes(MealType.Vegetarian),
      )
    }

    groupedByCategory = groupedByCategory.filter((g) => g.length !== 0)

    for (const group of groupedByCategory) {
      // Academica Express 0€ price check
      if (
        group[0].category === 'Express' &&
        group[0].price === null &&
        group[0].meals[0].name ===
          (mainDishes.find((d) => d.category === 'Klassiker')?.meals[0].name ??
            '')
      ) {
        message += `_${group[0].category}_ — ${currencyFormatter.format(mainDishes.find((d) => d.category === 'Klassiker')?.price ?? 0).replace(/\s/g, '')}\n`
      } else {
        message += `_${group[0].category}_ — ${currencyFormatter.format(group[0].price ?? 0).replace(/\s/g, '')}\n`
      }

      for (const dish of group) {
        let dishString: string =
          '*' +
          dish.meals
            .reduce(
              (prev, cur) =>
                prev +
                cur.name +
                (dish.meals.indexOf(cur) !== 0 ? getAllergenString(cur) : '') +
                ' | ',
              '',
            )
            .replace(/ \| $/gm, '')

        if (dishString.includes(' |')) {
          dishString = dishString.replace(
            ' |',
            '*' + getAllergenString(dish.meals[0]) + ' |',
          )
        } else {
          dishString += '*' + getAllergenString(dish.meals[0])
        }

        message +=
          dishString +
          ` ${dish.mealTypes.map((m) => mealTypeEmojis[m]).join(' ')}`

        const rating = await getMeanRating(prisma, dish)

        message += ` _– ⭐ ${rating.meanRating} (${rating.totalRatings})_\n`
      }
      message += '\n'
    }
  }

  if (sideDishes.length !== 0 && numberOfDishes === 0) {
    message +=
      'Dein gewählter Filter hat leider keine Ergebnisse geliefert! :(\n\n'
  }

  if (sideDishes.length === 0 && numberOfDishes === 0) {
    message += '*geschlossen*'
  } else {
    // derive mealTypes for side dishes with allergens
    const mainSideDishes: MealResponse[] = sideDishes.filter(
      (d) => d.sideDishType === SideDishType.Main,
    )
    const secondarySideDishes: MealResponse[] = sideDishes.filter(
      (d) => d.sideDishType === SideDishType.Secondary,
    )

    message += `_Hauptbeilagen:_ ${mainSideDishes.map((d) => `*${d.name}*${getAllergenString(d)}`).join(' oder ')}`
    message += ` _– ⭐ ${(
      await Promise.all(
        mainSideDishes.map(async (d) => {
          const rating = await getMeanRating(prisma, {
            meals: [d],
          } as MenuResponse)

          return `${rating.meanRating} (${rating.totalRatings})`
        }),
      )
    ).join(' und ')}_`

    message += `\n_Nebenbeilagen:_ ${secondarySideDishes.map((d) => `*${d.name}*${getAllergenString(d)}`).join(' oder ')}`
    message += ` _– ⭐ ${(
      await Promise.all(
        secondarySideDishes.map(async (d) => {
          const rating = await getMeanRating(prisma, {
            meals: [d],
          } as MenuResponse)

          return `${rating.meanRating} (${rating.totalRatings})`
        }),
      )
    ).join(' und ')}_`
  }
  return message
}

const handleDateCommand = async (
  command: string,
  match: RegExpExecArray,
  msg: TelegramBot.Message,
): Promise<void> => {
  switch (command) {
    case 'heute':
      savedDate = moment()
      break
    case 'morgen':
      savedDate = moment().add(1, 'day')
      break
    case 'uebermorgen':
      savedDate = moment().add(2, 'day')
      break
    case 'alles':
      savedDate = moment(0)
      break
  }

  if (match[2] === '') {
    awaitingCanteenInput = true

    await requestCanteenName(msg)
  } else if (
    mealTypeContains(match[2].trim().split(' ')[0]) ||
    ['vegetarisch', 'fleisch', 'meat'].includes(
      match[2].trim().split(' ')[0].toLowerCase(),
    )
  ) {
    awaitingCanteenInput = true
    savedFilter = match[2].trim().toLowerCase()

    await requestCanteenName(msg)
  } else if (
    readableCanteenNames
      .map((c) => c.toLowerCase())
      .filter((c) => c.includes(match[2].trim().split(' ')[0].toLowerCase()))
      .length !== 0
  ) {
    const matchedReadableCanteenName: string | undefined = readableCanteenNames
      .map((c) => c.toLowerCase())
      .find((c) => c.includes(match[2].trim().split(' ')[0].toLowerCase()))

    const canteenId: string = convertReadableCanteenToCanteenId(
      matchedReadableCanteenName ?? '',
    )

    if (
      match[2].trim().split(' ').at(1) !== undefined &&
      (mealTypeContains(match[2].trim().split(' ')[1]) ||
        ['vegetarisch', 'fleisch', 'meat'].includes(
          match[2].trim().split(' ')[1].toLowerCase(),
        ))
    ) {
      savedFilter = match[2].trim().split(' ')[1].toLowerCase()
    } else if (match[2].trim().split(' ').at(1) !== undefined) {
      // TODO: unrecognized filter
    }

    if (savedFilter !== '') {
      await sendMenu(msg, canteenId, savedDate, savedFilter)
      savedFilter = ''
    } else {
      await sendMenu(msg, canteenId, savedDate)
    }
  }
}

const handleFilterCommand = async (
  command: string,
  match: RegExpExecArray,
  msg: TelegramBot.Message,
): Promise<void> => {
  if (match[2] === '') {
    awaitingCanteenInput = true

    await requestCanteenName(msg)
  } else if (
    readableCanteenNames
      .map((c) => c.toLowerCase())
      .filter((c) => c.includes(match[2].trim().split(' ')[0].toLowerCase()))
      .length !== 0
  ) {
    const matchedReadableCanteenName: string | undefined = readableCanteenNames
      .map((c) => c.toLowerCase())
      .find((c) => c.includes(match[2].trim().split(' ')[0].toLowerCase()))

    const canteenId: string = convertReadableCanteenToCanteenId(
      matchedReadableCanteenName ?? '',
    )

    await sendMenu(msg, canteenId, savedDate, command)
  }
}

const handleTagCommand = async (
  match: RegExpExecArray,
  msg: TelegramBot.Message,
): Promise<void> => {
  const expectedTagFormats: string[] = ['dddd', 'DD.MM.YYYY', 'DD.MM.YY']
  let validFormat = ''
  if (match[2] === '') {
    await bot.sendMessage(
      msg.chat.id,
      'Bitte gib einen Tag oder ein Datum an!',
      { reply_to_message_id: msg.message_id },
    )
  } else {
    for (const format of expectedTagFormats) {
      if (moment(match[2].trim().split(' ')[0], format, true).isValid()) {
        validFormat = format
        break
      }
    }

    if (validFormat !== '') {
      const parsedMomentDate: moment.Moment = moment(
        match[2].trim().split(' ')[0],
        validFormat,
        true,
      )
      const modifiedMomentDate: moment.Moment = parsedMomentDate

      if (
        moment().week() === parsedMomentDate.week() &&
        moment().weekday() > parsedMomentDate.weekday()
      ) {
        modifiedMomentDate.add(1, 'week')
      }

      if (modifiedMomentDate.isSameOrAfter(moment(), 'day')) {
        if (match[2].trim().split(' ').length < 2) {
          awaitingCanteenInput = true

          savedDate = modifiedMomentDate

          await requestCanteenName(msg)
        } else if (
          readableCanteenNames
            .map((c) => c.toLowerCase())
            .filter((c) =>
              c.includes(match[2].trim().split(' ')[1].toLowerCase()),
            ).length !== 0
        ) {
          const matchedReadableCanteenName: string | undefined =
            readableCanteenNames
              .map((c) => c.toLowerCase())
              .find((c) =>
                c.includes(match[2].trim().split(' ')[1].toLowerCase()),
              )

          const canteenId: string = convertReadableCanteenToCanteenId(
            matchedReadableCanteenName ?? '',
          )

          if (
            match[2].trim().split(' ').length > 2 &&
            match[2].trim().split(' ').at(2) !== undefined
          ) {
            if (
              mealTypeContains(match[2].trim().split(' ')[2]) ||
              ['vegetarisch', 'fleisch', 'meat'].includes(
                match[2].trim().split(' ')[2].toLowerCase(),
              )
            ) {
              const filter: string = match[2].trim().split(' ')[2]
              await sendMenu(msg, canteenId, modifiedMomentDate, filter)
            }
          } else {
            await sendMenu(msg, canteenId, modifiedMomentDate)
          }
        }
      } else {
        await bot.sendMessage(
          msg.chat.id,
          'Gewählter Tag liegt in der Vergangenheit!',
          { reply_to_message_id: msg.message_id },
        )
      }
    } else {
      await bot.sendMessage(
        msg.chat.id,
        'Tages- oder Datumsformat nicht erkannt!',
        { reply_to_message_id: msg.message_id },
      )
    }
  }
}

const sendChannelMessage = async (): Promise<void> => {
  const today: moment.Moment = moment()

  const ahorn = await requestMenu(accessToken, 'Ahornstrasse', today)
  const vita = await requestMenu(accessToken, 'Vita', today)
  const aca = await requestMenu(accessToken, 'Academica', today)

  const prisma = new PrismaClient()

  if (aca.length !== 0 && ahorn.length !== 0 && vita.length !== 0) {
    const botUsername = (await bot.getMe()).username

    const markup: TelegramBot.InlineKeyboardMarkup = {
      inline_keyboard: [
        [
          { text: 'Ahorn', callback_data: 'ahorn' },
          { text: 'Academica', callback_data: 'aca' },
          { text: 'Vita', callback_data: 'vita' },
        ],
        [
          {
            text: 'Mehr Optionen via DMs',
            url: `tg://resolve?domain=${botUsername}`,
          },
        ],
        [
          {
            text: 'Gericht bewerten',
            url: `tg://resolve?domain=${botUsername}&start=${today.format('YYYY-MM-DD')}`,
          },
        ],
      ],
    }

    // we cant handle multiple messages with markups yet so we hope that there will never be a channel
    // message with more than 4096 characters
    const msgs: TelegramBot.Message[] = await sendMenu(
      null,
      'Ahornstrasse',
      today,
      '',
      markup,
    )

    await prisma.channelPost.createMany({
      data: msgs.map((msg) => ({
        channelId: msg.chat.id,
        messageId: msg.message_id,
        markupCleared: false,
        date: today.add(2, 'hours').toISOString(),
        currentCanteen: 'Ahornstrasse',
      })),
    })
  }
}

const handleAllergensCommand = async (
  msg: TelegramBot.Message,
): Promise<void> => {
  let msgStr = ''

  msgStr += '*Allergeninformationen:*\n\n'

  for (const key of Object.keys(allergenTypeEmojisAndStrings)) {
    const code: string =
      allergenTypeEmojisAndStrings[key][
        allergenTypeEmojisAndStrings[key].length - 2
      ]

    msgStr += code + ': ' + key + '\n'
  }

  await bot.sendMessage(msg.chat.id, msgStr, { parse_mode: 'Markdown' })
}

const handleRateCommand = async (
  match: RegExpExecArray,
  msg: TelegramBot.Message,
): Promise<void> => {
  const prisma = new PrismaClient()
  let validFormat = ''

  const expectedFormats: string[] = ['YYYY-MM-DD', 'DD.MM.YYYY', 'DD.MM.YY']

  for (const format of expectedFormats) {
    if (moment(match[2].trim(), format, true).isValid()) {
      validFormat = format
      break
    }
  }

  if (validFormat !== '') {
    const parsedDate = moment(match[2].trim(), validFormat)
    const mensaDay = await prisma.mensaDay.findFirst({
      where: { date: { equals: parsedDate.add(2, 'hours').toISOString() } },
      select: { date: true, meals: true },
    })

    if (mensaDay !== null) {
      const meals = mensaDay.meals.sort((a, b) =>
        a.dishName.localeCompare(b.dishName),
      )

      await bot.sendMessage(
        msg.chat.id,
        'An diesem Tag gab es die folgenden Gerichte in den StW-Mensen. Bitte wähle eines aus, um es zu bewerten:',
        {
          reply_to_message_id: msg.message_id,
          reply_markup: {
            inline_keyboard: meals.map((meal) => {
              const mealTypeEmoji =
                meal.mealTypes.includes('Vegan') ? mealTypeEmojis.Vegan
                : meal.mealTypes.length > 0 ?
                  mealTypeEmojis[meal.mealTypes[0] as MealType]
                : ''
              return [
                {
                  text: `${mealTypeEmoji} ${meal.dishName}${meal.subDishNames.length > 0 ? ' mit ' + meal.subDishNames.join(', ') : ''}`,
                  callback_data: `ratestart ${meal.id}`,
                },
              ]
            }),
          },
        },
      )
    } else {
      await bot.sendMessage(
        msg.chat.id,
        `Zu diesem Tag habe ich leider keine verzeichneten Gerichte. 😞${parsedDate.isAfter(moment()) ? ' (Nur aktuelle/vergangene Mensatage unterstützt.)' : ''}`,
        {
          reply_to_message_id: msg.message_id,
        },
      )
    }
  } else {
    await bot.sendMessage(msg.chat.id, 'Datumsformat nicht erkannt!', {
      reply_to_message_id: msg.message_id,
    })
  }
}

const handleRatingsCommand = async (
  msg: TelegramBot.Message,
): Promise<void> => {
  const prisma = new PrismaClient()

  const ratings = await prisma.rating.findMany({
    where: { userId: msg.from?.id },
    include: {
      dish: { include: { mensaDays: { orderBy: { date: 'desc' }, take: 1 } } },
    },
  })
  const ratingsByMensaDay: {
    date: string
    ratings: { dishName: string; subDishNames: string[]; rating: number }[]
  }[] = []

  if (ratings.length > 0) {
    for (const rate of ratings.sort((a, b) =>
      a.dish.dishName.localeCompare(b.dish.dishName),
    )) {
      const mensaDay = ratingsByMensaDay.find(
        (rating) =>
          rating.date ===
          moment(rate.dish.mensaDays[0].date).format('YYYY-MM-DD'),
      )
      if (mensaDay === undefined) {
        ratingsByMensaDay.push({
          date: moment(rate.dish.mensaDays[0].date).format('YYYY-MM-DD'),
          ratings: [
            {
              dishName: rate.dish.dishName,
              subDishNames: rate.dish.subDishNames,
              rating: rate.rating,
            },
          ],
        })
      } else {
        ratingsByMensaDay[ratingsByMensaDay.indexOf(mensaDay)].ratings.push({
          dishName: rate.dish.dishName,
          subDishNames: rate.dish.subDishNames,
          rating: rate.rating,
        })
      }
    }

    const ratingsString = ratingsByMensaDay
      .map(
        (ratingDay) =>
          `_${moment(ratingDay.date).format('DD.MM.YY')}:_\n${ratingDay.ratings
            .map((rating) => {
              const dishRatingString =
                rating.dishName +
                (rating.subDishNames.length > 0 ?
                  ' mit ' + rating.subDishNames.join(', ')
                : '')

              return (
                (dishRatingString.length > 42 ?
                  dishRatingString.slice(0, 42).trim() + '…'
                : dishRatingString) + `: *${rating.rating}*`
              )
            })
            .join('\n')}`,
      )
      .join('\n\n')

    await bot.sendMessage(
      msg.chat.id,
      'Hier sind deine Bewertungen, absteigend sortiert nach dem Datum, ' +
        'an welchem das zugehörige Gericht zuletzt in den Mensen angeboten wurde. ' +
        ' Um sie zu bearbeiten, kannst du einfach `/rate {datum}` verwenden.\n\n' +
        ratingsString,
      {
        reply_to_message_id: msg.message_id,
        parse_mode: 'Markdown',
      },
    )
  } else {
    await bot.sendMessage(
      msg.chat.id,
      'Du hast noch gar keine Bewertungen abgegeben!',
      { reply_to_message_id: msg.message_id },
    )
  }
}

const clearMarkup = async (): Promise<void> => {
  const prisma = new PrismaClient()
  const channelPosts = await prisma.channelPost.findMany({
    where: { markupCleared: false },
  })
  const botUsername = (await bot.getMe()).username

  for (const post of channelPosts) {
    await bot.editMessageReplyMarkup(
      {
        inline_keyboard: [
          [
            {
              text: 'Gericht bewerten',
              url: `tg://resolve?domain=${botUsername}&start=${moment(post.date).format('YYYY-MM-DD')}`,
            },
          ],
        ],
      },
      { message_id: post.messageId, chat_id: Number(post.channelId) },
    )

    await prisma.channelPost.update({
      where: { id: post.id },
      data: { markupCleared: true },
    })
  }
}

const getAllergenString = (meal: MealResponse): string => {
  let allergenString = ''

  const allergenStrings: string[] = Object.keys(
    allergenTypeEmojisAndStrings,
  ).filter((a) => meal.allergens.includes(a))

  const emojiAllergenStrings: string[] = Object.keys(
    allergenTypeEmojisAndStrings,
  ).filter(
    (a) =>
      meal.allergens.includes(a) && allergenTypeEmojisAndStrings[a].length > 2,
  )

  for (const str of emojiAllergenStrings) {
    if (str !== 'Fische') {
      allergenString += allergenTypeEmojisAndStrings[str][0] + ' '
    }
  }

  for (const str of allergenStrings) {
    allergenString +=
      allergenTypeEmojisAndStrings[str][
        allergenTypeEmojisAndStrings[str].length - 1
      ] + ' '
  }

  if (allergenString !== '') {
    allergenString = ' ' + allergenString
  }

  return allergenString.replace(/ $/, '')
}

const saveMenus = async (): Promise<void> => {
  const prisma = new PrismaClient()
  const today: moment.Moment = moment()
  const weekMenus = await requestAllMenus(accessToken, today)

  const menus: MenuOfTheDayResponse[] = []

  menus.push(...weekMenus.map((weekMenu) => weekMenu.menusOfTheDay).flat())

  const updates: {
    mealTypes: string[]
    dishName: string
    subDishNames: string[]
    isSingularSideDish: boolean
    category: string
  }[] = []

  const dbMeals = await prisma.meal.findMany()
  const foundDBMealIds: number[] = []

  // All Meals

  // sideDishes (a.k.a. "Sättigungsbeilagen")
  for (const menu of menus) {
    updates.push(
      ...menu.sideDishes.map((sideDish) => ({
        mealTypes: [],
        dishName: sideDish.name,
        subDishNames: [],
        isSingularSideDish: true,
        category:
          sideDish.sideDishType === SideDishType.Main ?
            'Hauptbeilage'
          : 'Nebenbeilage',
      })),
    )
  }

  // Actual Dishes
  for (const menu of menus.map((canteenMenu) => canteenMenu.menus).flat()) {
    if (
      !menu.meals.some(
        (meal) =>
          meal.name.toLowerCase().includes('geschlossen') ||
          meal.name.toLowerCase().includes('ausverkauft') ||
          meal.name.toLowerCase().includes('heute kein angebot') ||
          meal.name.toLowerCase().includes('streik'),
      ) &&
      menu.meals.length !== 0
    ) {
      const newLength = updates.push({
        mealTypes: menu.mealTypes,
        dishName: menu.meals[0].name,
        subDishNames: [],
        isSingularSideDish: false,
        category: menu.category,
      })

      updates[newLength - 1].subDishNames.push(
        ...menu.meals.slice(1).map((meal) => meal.name),
      )
    }
  }

  // Filter Duplicates
  const seen = new Set<string>(
    dbMeals.map(
      (meal) => `${meal.dishName}:${[...meal.subDishNames].sort().join()}`,
    ),
  )

  const updatesFiltered = updates.filter((update) => {
    const key = `${update.dishName}:${[...update.subDishNames].sort().join()}`

    if (seen.has(key)) {
      const dbMeal = dbMeals.find(
        (meal) =>
          meal.dishName === update.dishName &&
          meal.subDishNames.sort().join() === update.subDishNames.sort().join(),
      )
      if (dbMeal !== undefined) {
        foundDBMealIds.push(dbMeal.id)
      }
      return false
    }

    seen.add(key)
    return true
  })

  const newRows = await prisma.meal.createManyAndReturn({
    data: updatesFiltered,
  })

  if (newRows.length + foundDBMealIds.length !== 0) {
    // Mensa Day
    await prisma.mensaDay.upsert({
      create: {
        date: today.add(2, 'hours').toISOString(),
        meals: {
          connect: [
            ...newRows.map((row) => ({ id: row.id })),
            ...foundDBMealIds.map((id) => ({ id })),
          ],
        },
      },
      update: {},
      where: { date: today.add(2, 'hours').toDate() },
    })
  }
}

cron.schedule('0 8 * * *', async () => await sendChannelMessage(), {
  runOnInit: false,
  timezone: 'Europe/Berlin',
})
cron.schedule('0 0 * * *', async () => await clearMarkup(), {
  runOnInit: false,
  timezone: 'Europe/Berlin',
})
cron.schedule('55 7 * * *', async () => await saveMenus(), {
  runOnInit: false,
  timezone: 'Europe/Berlin',
})
