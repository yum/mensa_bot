import moment from 'moment-timezone'
import {
  MealType,
  MenuResponse,
  TokenRefreshResponse,
  TokenValidationResponse,
  type MenuOfTheDayResponse,
  type WeekMenuResponse,
} from './responses.js'
import { PrismaClient } from '@prisma/client'
import { bot, getMessageString } from './bot.js'
import TelegramBot from 'node-telegram-bot-api'
moment.tz.setDefault('Europe/Berlin')
moment.locale('de-DE')

export const refreshAccessToken = async (): Promise<string> => {
  const response: Response = await fetch(
    'https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/token',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        client_id: process.env.CLIENT_ID,
        refresh_token: process.env.REFRESH_TOKEN,
        grant_type: 'refresh_token',
      }),
    },
  )

  if (response.ok && response.status === 200) {
    return ((await response.json()) as TokenRefreshResponse).access_token
  } else {
    console.error('Error while refreshing access token:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return ''
  }
}

export const validateAccessToken = async (
  accessToken: string,
): Promise<boolean> => {
  const response: Response = await fetch(
    'https://oauth.campus.rwth-aachen.de/oauth2waitress/oauth2.svc/tokeninfo',
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        client_id: process.env.CLIENT_ID,
        access_token: accessToken,
      }),
    },
  )

  if (response.ok && response.status === 200) {
    return (
      ((await response.json()) as TokenValidationResponse).state === 'valid'
    )
  } else {
    console.error('Error while validating access token:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return false
  }
}

export const requestCanteens = async (
  accessToken: string,
): Promise<string[]> => {
  if (!(await validateAccessToken(accessToken))) {
    accessToken = await refreshAccessToken()
  }

  const response: Response = await fetch(
    'https://api.app.rwth-aachen.de/v3/canteen/id',
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: accessToken,
      },
    },
  )

  if (response.ok && response.status === 200) {
    return (await response.json()) as string[]
  } else {
    console.error('Error while querying canteens list:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return []
  }
}

export const getCanteenIds = async (accessToken: string): Promise<string[]> =>
  await requestCanteens(accessToken)

export const getReadableCanteenNames = (canteens: string[]): string[] => {
  return canteens.map((c) => {
    switch (c) {
      case 'Templergraben':
        return 'Bistro Templergraben'
      case 'Esstw':
        return 'Esstw'
      default:
        return `Mensa ${c.replace('strasse', 'straße').replace('ue', 'ü')}`
    }
  })
}

export const convertReadableCanteenToCanteenId = (query: string): string => {
  query = query.toLowerCase()

  if (query.includes('bistro')) {
    return 'Templergraben'
  }

  return capitalize(
    query.replace('mensa ', '').replace('straße', 'strasse').replace('ü', 'ue'),
  )
}

export const requestMenu = async (
  accessToken: string,
  canteen: string,
  date?: moment.Moment,
): Promise<MenuOfTheDayResponse[]> => {
  if (!(await validateAccessToken(accessToken))) {
    accessToken = await refreshAccessToken()
  }

  const response: Response = await fetch(
    'https://api.app.rwth-aachen.de/v3/canteen/weekMenu?' +
      new URLSearchParams({
        lang: 'de',
        filter: `canteen/id eq '${canteen}'`,
      }).toString(),
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: accessToken,
      },
    },
  )

  if (response.ok && response.status === 200) {
    const resp: WeekMenuResponse[] =
      (await response.json()) as WeekMenuResponse[]

    if (date !== undefined) {
      const convDate: string = date.format('YYYY-MM-DD')

      return resp[0].menusOfTheDay.filter((m) => m.day === convDate)
    } else {
      // "alles", but only future menus
      return resp[0].menusOfTheDay.filter((m) =>
        moment(m.day).isSameOrAfter(moment(), 'day'),
      )
    }
  } else {
    console.error('Error while querying menu:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return []
  }
}

export const requestAllMenus = async (
  accessToken: string,
  date?: moment.Moment,
): Promise<WeekMenuResponse[]> => {
  if (!(await validateAccessToken(accessToken))) {
    accessToken = await refreshAccessToken()
  }

  const response: Response = await fetch(
    'https://api.app.rwth-aachen.de/v3/canteen/weekMenu?' +
      new URLSearchParams({
        lang: 'de',
      }).toString(),
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: accessToken,
      },
    },
  )

  if (response.ok && response.status === 200) {
    const resp: WeekMenuResponse[] =
      (await response.json()) as WeekMenuResponse[]

    if (date !== undefined) {
      const convDate: string = date.format('YYYY-MM-DD')

      return resp.map((weekMenu) => ({
        ...weekMenu,
        menusOfTheDay: weekMenu.menusOfTheDay.filter(
          (dayMenu) => dayMenu.day === convDate,
        ),
      }))
    } else {
      // "alles", but only future menus
      return resp.map((weekMenu) => ({
        ...weekMenu,
        menusOfTheDay: weekMenu.menusOfTheDay.filter((dayMenu) =>
          moment(dayMenu.day).isSameOrAfter(moment(), 'day'),
        ),
      }))
    }
  } else {
    console.error('Error while querying menu:', response.statusText)

    const error: Error = new Error(response.statusText)
    await Promise.reject(error)
    return []
  }
}

export const getMeanRating = async (
  prisma: PrismaClient,
  dish: MenuResponse,
): Promise<{ meanRating: string; totalRatings: number }> => {
  let dishRatings = await prisma.rating.findMany({
    where: {
      dish: {
        dishName: dish.meals[0].name,
        subDishNames: {
          hasEvery: dish.meals.slice(1).map((meal) => meal.name),
        },
      },
    },
    select: { dish: true, rating: true },
  })

  // The above query doesnt get the correct ratings if the meal exists with and without subDishes (subDishNames: [])
  // because the hasEvery filter is always true for [] (only subseteq check)
  dishRatings = dishRatings.filter(
    (rating) => rating.dish.subDishNames.length === dish.meals.length - 1,
  )

  const sumRating = dishRatings.reduce((prev, cur) => prev + cur.rating, 0)

  return {
    meanRating: (sumRating === 0 ? 0 : sumRating / dishRatings.length
    ).toLocaleString('de-DE', { maximumFractionDigits: 1 }),
    totalRatings: dishRatings.length,
  }
}

export const refreshChannelPostRating = async (
  dishId: number,
  markup?: TelegramBot.InlineKeyboardMarkup,
): Promise<void> => {
  const prisma = new PrismaClient()
  const mensaDay = await prisma.mensaDay.findFirst({
    orderBy: { date: 'desc' },
    where: { meals: { some: { id: dishId } } },
  })

  if (mensaDay !== null) {
    const channelPosts = await prisma.channelPost.findMany({
      where: { date: mensaDay.date, markupCleared: false },
    })

    for (const post of channelPosts) {
      const newString = await getMessageString(
        post.currentCanteen ?? '',
        moment(mensaDay.date, 'YYYY-MM-DD'),
      )

      try {
        // This may fail if the message is identical
        await bot.editMessageText(newString, {
          parse_mode: 'Markdown',
          message_id: post.messageId,
          chat_id: Number(post.channelId),
          reply_markup: markup,
        })
      } catch (e) {
        console.error('Error while editing Channel message', e)
      }
    }
  }
}

export const capitalize = (str: string): string => {
  return str.charAt(0).toUpperCase() + str.slice(1, str.length)
}

export const mealTypeContains = (str: string): boolean => {
  return Object.keys(MealType)
    .map((m) => m.toLowerCase())
    .includes(str.toLowerCase())
}

export const unMarkdownify = (str: string): string => {
  return str.replaceAll(/\*|_/g, '').trim()
}
